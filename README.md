#### Running it locally ####
* Clone the app to `mysqlProjectsList` folder
* `cd mysqlProjectsList` folder
* `npm install`
* `node app.js`
* open browser at `localhost:3000`

### Credits ###
 * Front-end is from AngularJS example.


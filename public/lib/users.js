app.config(function($routeProvider) {
          $routeProvider.
            when('/users', {controller:ListCtrl, templateUrl:'../templates/list.html'}).
            when('/users/edit/:projectId', {controller:EditCtrl, templateUrl:'../templates/detail.html'}).
            when('/users/new', {controller:CreateCtrl, templateUrl:'../templates/register.html'}).
            otherwise({redirectTo:'/'});
        });